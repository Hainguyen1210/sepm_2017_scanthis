const urlParser = require('url');
const bodyParser = require('body-parser');
const express = require('express');
const assert = require('assert');
const randomName = require('random-name');
const mongo = require('mongodb');
const dateFormat = require('dateformat');
const multer = require('multer');
const mongoClient = mongo.MongoClient;
const app = express();
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(express.static(__dirname + '/public')); // set static folder to serve images

var now = new Date();
console.log(dateFormat(now, "yyyymmddHHMMssl"));

// Connection URL
const url = 'mongodb://localhost:27017/sepm';
const port = 3000;

const collections = ["products"]

// Create needed collections
mongoClient.connect(url, function(err, db){
    assert.equal(null, err);
    console.log("connected successfully to server");
    db.createCollection( collections[0], function() {
        if (err) throw err;
        console.log("collection created!");
        db.close();
    });
});

app.get('/product', function(req, res){
    const parameters = urlParser.parse(req.url, true).query;
    console.log("on /product PARAMETER: " + JSON.stringify(parameters));
    mongoClient.connect(url, function(err, db){
        assert.equal(null, err);
        db.collection(collections[0]).find( { code: parseInt(parameters.code)}).toArray(function(err, result){
            db.close();
            if (err) {
                console.log("something isn't right")
                return;
            };
            res.send(JSON.stringify(result));
        });
    });
});

app.post('/addProduct', function(req, res){
    const reqBody = req.body;
    console.log("on /addProduct PARAMETER: " + JSON.stringify(reqBody));
    const addingProduct = {
        name: reqBody.name,
        code: parseInt(reqBody.code),
        imageUrl: reqBody.imageUrl,
        places: [{   
            price: parseFloat(reqBody.price),
            name: reqBody.placeName,
            lat: parseFloat(reqBody.lat),
            lon: parseFloat(reqBody.lon)
        }]
    };

    mongoClient.connect(url, function(err, db){
        assert.equal(null, err);
        db.collection(collections[0]).insertOne(addingProduct, function(err, res){
            db.close();
            if (err) {
                res.status(500).send("adding failed");
                console.log(err);
                return;
            };
        });
    });
    res.status(200).send("product added");
});


// UPDATE PRICES
app.post('/updatePrice', function(req, res){
    const reqBody = req.body;
    mongoClient.connect(url, function(err, db){
        assert.equal(null, err);
        // construct the new price
        const addingPrice = {
            price: parseFloat(reqBody.price),
            name: reqBody.placeName,
            lat: parseFloat(reqBody.lat),
            lon: parseFloat(reqBody.lon)
        };
        db.collection(collections[0]).updateOne(
            { code: parseInt(reqBody.code) },
            { $push: { places: addingPrice } },
            function (err, result){
                db.close();
                if (err) {
                    res.status(500).send("that price has not been added", err);
                    console.log(err);
                    return;
                }
            }
        );
    });
    res.status(200).send("new price added");
});

// IMAGE UPLOAD
app.post('/image', function (req, res) {
    const storedFilename = dateFormat(now, "yyyymmddHHMMssl") + '.jpg';
    const Storage = multer.diskStorage({
        destination: function (req, file, callback) {
            callback(null, __dirname + '/public/images');
        },
        filename: function (req, file, callback) {
            callback(null, storedFilename);
        }
    });
    const upload = multer({ storage: Storage }).single('productImage');
    
    upload(req, res, function (err) {
        if (err) {
            console.log(err);
            return res.status(500).send("Something went wrong !");
        }
        res.status(200).send("http://188.166.209.81:3000/images/" + storedFilename);
        return res.end();
    });
});

app.listen(port, function() {
    console.log("waiting at port " + String(port));
});