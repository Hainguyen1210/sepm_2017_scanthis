package com.example.leovo.scanthisbarcoder;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.StrictMode;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlacePicker;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Method;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class UploadActivity extends AppCompatActivity {
    final String TAG = "pipi";
    boolean isOpeningCamera = false,
            isOpeningPlacePicker = false,
            isLocationEmpty = true,
            isImageEmpty = true;
    private String barcode;
    private Product addingProduct;
    private Shop addingShop;
    private ImageView imageLocation, imageProduct;
    private TextView viewProductId, textViewShopAddress;
    private EditText editTextProductName, editTextProductPrice, editTextProductDesc;
    private ProgressBar progressBarUploadImage;
    private Service service;
    private ImageHandler imageHandler;
    private ProgressDialog progressDialog;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_upload);


        if(Build.VERSION.SDK_INT>=24){
            try{
                Method m = StrictMode.class.getMethod("disableDeathOnFileUriExposure");
                m.invoke(null);
            }catch(Exception e){
                e.printStackTrace();
            }
        }

        // Image uploader
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor).build();
        service = new Retrofit.Builder().baseUrl("http://188.166.209.81:3000/").client(client).build().create(Service.class);

        // initiate product
        addingProduct = new Product();
        addingShop = new Shop();
        imageHandler = new ImageHandler(getApplicationContext(), this, MainActivity.REQUEST_PERMISSION_CODE_CAMERA);

        try{
            barcode = getIntent().getExtras().getString(MainActivity.PUTEXTRA_BARCODE);
            addingProduct.setCode(barcode);
        } catch (NullPointerException e){
            Toast.makeText(this, "no barcode detected", Toast.LENGTH_SHORT).show();
            this.finish();
        }
        viewProductId = (TextView) findViewById(R.id.view_product_code);
        viewProductId.setText(barcode);
        textViewShopAddress = (TextView) findViewById(R.id.view_location_address);
        editTextProductName = (EditText) findViewById(R.id.edit_product_name);
        editTextProductName.requestFocus();
        editTextProductDesc = (EditText) findViewById(R.id.edit_product_description);
        editTextProductPrice = (EditText) findViewById(R.id.edit_product_price);
        editTextProductPrice.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                editTextProductPrice.setHint(b? "":"VND");
            }
        });

        Button buttonSelectLocation = (Button) findViewById(R.id.button_select_location);
        buttonSelectLocation.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                pickLocation();
            }
        });

        Button buttonSelectImage = (Button) findViewById(R.id.button_select_image);
        buttonSelectImage.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                selectImage();
            }
        });

        Button buttonSubmit = (Button) findViewById(R.id.button_submit);
        buttonSubmit.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                submitProduct();
            }
        });

        imageLocation = (ImageView) findViewById(R.id.image_location);
        imageProduct = (ImageView) findViewById(R.id.image_product);
        progressBarUploadImage = (ProgressBar) findViewById(R.id.progress_uploadImage);
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Uploading product");
    }

    private void submitProduct() {
        if (editTextProductName.getText().length() < 1) {
            editTextProductName.requestFocus();
            return;
        } else {
            addingProduct.setName(editTextProductName.getText().toString());
        }
        if (editTextProductPrice.getText().toString().equals("")) {
            editTextProductPrice.requestFocus();
            return;
        } else {
            addingShop.setPrice(Double.parseDouble(editTextProductPrice.getText().toString()));
        }
        if (editTextProductDesc.getText().length() < 1){
            editTextProductDesc.requestFocus();
            return;
        }else{
            addingProduct.setDesc(editTextProductDesc.getText().toString());
        }

        if (isLocationEmpty){
            Toast.makeText(this, "Taking Location", Toast.LENGTH_SHORT).show();
            pickLocation();
            return;
        }
        if (isImageEmpty) {
            Toast.makeText(this, "Taking Image", Toast.LENGTH_SHORT).show();
            selectImage();
            return;
        }
        progressDialog.show();
        @SuppressLint("StaticFieldLeak") Fetcher fetcher = new Fetcher() {
            @Override
            protected void onFetched(String res) {
                Toast.makeText(UploadActivity.this, res, Toast.LENGTH_SHORT).show();
                progressDialog.dismiss();
                Log.d(TAG, "onFetched: " + res);
                if (res.contains("product added")){
                    finish();
                }
            }
        };
        StringBuilder paramsBuilder = new StringBuilder();
        paramsBuilder.append("name=").append(addingProduct.getName());
        paramsBuilder.append("&desc=").append(addingProduct.getDesc());
        paramsBuilder.append("&code=").append(addingProduct.getCode());
        paramsBuilder.append("&imageUrl=").append(addingProduct.getImageUrl());
        paramsBuilder.append("&price=").append(addingShop.getPrice());
        paramsBuilder.append("&placeName=").append(addingShop.getName());
        paramsBuilder.append("&lat=").append(addingShop.getLat());
        paramsBuilder.append("&lon=").append(addingShop.getLon());
        fetcher.execute("http://188.166.209.81:3000/addProduct", "POST", paramsBuilder.toString());
    }

    private void selectImage() {
        if (isOpeningCamera) return;
        isOpeningCamera = true;
        isImageEmpty = true;
        // CAMERA INTENT
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, MainActivity.REQUEST_PERMISSION_CODE_LOCATION);
        };
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, MainActivity.REQUEST_PERMISSION_CODE_LOCATION);
        };
        if (ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED){
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CAMERA}, MainActivity.REQUEST_PERMISSION_CODE_CAMERA);
        }
//        Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
//        startActivityForResult(cameraIntent, REQUEST_PERMISSION_CODE_CAMERA);
        imageHandler.startIntent();
    }

    private void pickLocation() {
        if (isOpeningPlacePicker) return;
        isOpeningPlacePicker = true;
        isLocationEmpty = true;
        if (ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED){
            try {
                PlacePicker.IntentBuilder builder = new PlacePicker.IntentBuilder();
                startActivityForResult(builder.build(this), MainActivity.REQUEST_PERMISSION_CODE_LOCATION);
            } catch (GooglePlayServicesRepairableException e) {
                Log.d(TAG, "pickLocation: ");
                e.printStackTrace();
            } catch (GooglePlayServicesNotAvailableException e) {
                Log.d(TAG, "pickLocation: ");
                e.printStackTrace();
            }
        } else {
            finish();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode){
            case MainActivity.REQUEST_PERMISSION_CODE_LOCATION:
                isOpeningPlacePicker = false;
                if (resultCode == RESULT_OK) {
                    Place place = PlacePicker.getPlace(this, data);
                    String address = place.getName() + ", " + place.getAddress();
                    if (place.getName().toString().contains("°")){
                        Toast.makeText(this, "Tap on a shop to get exact name", Toast.LENGTH_LONG).show();
                        pickLocation();
                    } else {
                        addingShop.setName(address);
                        addingShop.setLat(place.getLatLng().latitude);
                        addingShop.setLon(place.getLatLng().longitude);
                        Picasso.with(getApplicationContext())
                                .load("https://maps.googleapis.com/maps/api/staticmap?center="
                                        + addingShop.getLat() + ","
                                        + addingShop.getLon() + "&zoom=18&size=800x400&key=AIzaSyBvN1dMwPYBGdaJ42U8S4GNM7UVlk_LHAc")
                                .into(imageLocation);
                        textViewShopAddress.setText(address);
                        isLocationEmpty = false;
                    }
                }
                break;
            case MainActivity.REQUEST_PERMISSION_CODE_CAMERA:
                isOpeningCamera = false;
                if (resultCode == RESULT_OK){
                    progressBarUploadImage.setVisibility(View.VISIBLE);
                    imageProduct.setVisibility(View.GONE);

                    File file = imageHandler.getFile();
                    // Upload image
                    RequestBody reqFile = RequestBody.create(MediaType.parse("image/*"), file);
                    MultipartBody.Part body = MultipartBody.Part.createFormData("productImage", file.getName(), reqFile);
                    retrofit2.Call<okhttp3.ResponseBody> req = service.postImage(body);
                    req.enqueue(new Callback<okhttp3.ResponseBody>() {
                        @Override
                        public void onResponse(@NonNull Call<okhttp3.ResponseBody> call, @NonNull Response<okhttp3.ResponseBody> response) {
                            // Display into view
                            if (response.isSuccessful()){
                                try {
                                    imageHandler.deleteTempFile();
                                    addingProduct.setImageUrl(response.body().source().readUtf8());
                                    Toast.makeText(UploadActivity.this, addingProduct.getImageUrl(), Toast.LENGTH_SHORT).show();
                                    Log.d(TAG, addingProduct.getImageUrl());
                                    Picasso.with(getApplicationContext()).load(addingProduct.getImageUrl())
                                            .into(imageProduct);
                                    progressBarUploadImage.setVisibility(View.GONE);
                                    imageProduct.setVisibility(View.VISIBLE);
                                    isImageEmpty = false;
                                } catch (IOException e) {
                                    e.printStackTrace();
                                }
                            }
                        }
                        @Override
                        public void onFailure(@NonNull Call<ResponseBody> call, @NonNull Throwable t) {
                            t.printStackTrace();
                        }
                    });
                }
                break;
        }
    }
}
