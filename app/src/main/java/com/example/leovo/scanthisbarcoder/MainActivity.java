package com.example.leovo.scanthisbarcoder;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.util.Log;
import android.view.ContextMenu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.api.CommonStatusCodes;
import com.google.android.gms.vision.barcode.Barcode;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

import at.markushi.ui.CircleButton;

public class MainActivity extends Activity {
//>>>>>>> 07f110a8b06ed56d3c3d17c5a4746bf4ce0f4f98:app/src/main/java/com/example/leovo/scanthisbarcoder/MainActivity.java
    TextView barcodeResult;
    final String TAG =  "pipi";
    static final String PUTEXTRA_BARCODE = "barcode",
                        PUTEXTRA_NAME = "productName",
                        PUTEXTRA_DESC = "productDesc";


    static final int
            REQUEST_CODE_CAMERA = 0,
            REQUEST_PERMISSION_CODE_LOCATION = 1,
            REQUEST_PERMISSION_CODE_CAMERA = 2,
            ADD_PRICE_INTENT = 3;

    private String result;
    private String currentImageUrl = "";
    private double lat = 0;
    private double lon = 0;

    private TextView viewProductName, viewProductCode, viewProductPlace, viewProductPrice, viewProductDescrip;
    private Button buttonAddPrice;
    private ListView lv_shops;
    private int currentSelectedShopIndex = 0;
    String currentBarcode;
    private Product currentProduct = null;
    private Gson gson = new Gson();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button button = (Button) findViewById(R.id.button_scan_barcode);

            button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    scanBarcode();
                }
            });



        CircleButton buttonViewPic = (CircleButton) findViewById(R.id.button_viewPic_context);
        buttonViewPic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                changetoContextPic();
            }
        });

        CircleButton popupview = (CircleButton) findViewById(R.id.button_descriptions);
        popupview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                changetoPopup();
            }
        });


        viewProductName = (TextView) findViewById(R.id.view_product_name);
        viewProductCode = (TextView) findViewById(R.id.view_product_code);
        viewProductPrice = (TextView) findViewById(R.id.view_product_price);
        viewProductDescrip = (TextView) findViewById(R.id.view_label_descrip);
        viewProductName.setText("");
        lv_shops = (ListView) findViewById(R.id.lv_shops);
        buttonAddPrice = (Button) findViewById(R.id.button_add_price);
        buttonAddPrice.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                addPriceForm(currentProduct);
            }
        });
//        viewProductPrice = findViewById(R.id.view_product_price);
        viewProductName.setText("");
        lv_shops = (ListView) findViewById(R.id.lv_shops);
        lv_shops.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                showShopLocation(currentProduct.getPlaces()[i].getLat(), currentProduct.getPlaces()[i].getLon());
            }
        });
        lv_shops.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                currentSelectedShopIndex = i;
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        Log.d(TAG, "onCreate: test ");
//        uploadProductForm("0123456789");
//        searchForProduct("0123456789");
        scanBarcode();


    }
    public boolean isNetworkAvailable() {
        ConnectivityManager connectivityMgr = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connectivityMgr.getActiveNetworkInfo();
        /// if no network is available networkInfo will be null
        if (networkInfo != null && networkInfo.isConnected()) {
            return true;
        }
        return false;
    }
    private void changetoPopup(){
        if(currentProduct != null){
            Log.d(TAG, "changetoPopup: "+ currentProduct.getDesc());
            Intent intent = new Intent(this,ViewProductDescActivity.class);
            intent.putExtra(PUTEXTRA_DESC, currentProduct.getDesc());
            startActivity(intent);
        }else{
            AlertDialog.Builder builder2 = new AlertDialog.Builder(this);
            builder2.setTitle("No Product Found");
            builder2.setMessage("No Product Found, Scanning is needed");
            builder2.setNegativeButton("Okay", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    Toast.makeText(MainActivity.this, "You need to Scan a Product", Toast.LENGTH_LONG).show();
                }
            });
            builder2.create().show();
        }
    }

    private void changetoContextPic(){
        if(currentImageUrl.equals("")){
            AlertDialog.Builder builder2 = new AlertDialog.Builder(this);
            builder2.setTitle("No Product Found");
            builder2.setMessage("No Product Found, Scanning is needed");
            builder2.setNegativeButton("Okay", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    Toast.makeText(MainActivity.this, "You need to Scan a Product", Toast.LENGTH_LONG).show();
                }
            });
            builder2.create().show();
        }
        else {
            Intent intent = new Intent(this, ViewProductPicActivity.class);
            intent.putExtra("image_url", currentImageUrl);
            startActivity(intent);
        }
    }

    private void scanBarcode(){
        if (isNetworkAvailable() == false) {
            AlertDialog.Builder builder =
                    new AlertDialog.Builder(this, R.style.Theme_AppCompat_DayNight_Dialog_Alert);
            builder.setTitle("No Internet Connection!");
            builder.setMessage("Please connect to a network to continue");
            builder.setNegativeButton("Close", null);
            builder.show();
        }else {
            if (ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED) {
                Intent intent = new Intent(this, ScanActivity.class);
                startActivityForResult(intent, REQUEST_CODE_CAMERA);
            } else {
                askPermissions();
            }
        }
    }

    private void uploadProductForm(String barcode){
        if (ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED){
            Intent intent = new Intent(this, UploadActivity.class);
            intent.putExtra(PUTEXTRA_BARCODE, barcode);
            startActivity(intent);
        } else {
            askPermissions();
        }
    }

    private void addPriceForm(Product product){
        if (product == null || product.getCode().equals("") || product.getName().equals("")) return;
        Intent intent = new Intent(this, AddPriceActivity.class);
        intent.putExtra(PUTEXTRA_BARCODE, currentBarcode);
        intent.putExtra(PUTEXTRA_NAME, product.getName());
        startActivityForResult(intent, ADD_PRICE_INTENT);
    }
    private void showShopLocation(double lat, double lon){
        Intent intent = new Intent(this, ShopLocationActivity.class);
        intent.putExtra("lat", lat);
        intent.putExtra("lon", lon);
        startActivity(intent);
    }

    private void askToUpload(final String barcode){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Upload product's details");
        builder.setMessage("The scanned product has not yet been added to database. Would you like to add its information?");
        builder.setPositiveButton("Sure", new DialogInterface.OnClickListener(){
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                uploadProductForm(barcode);
            }
        });
        builder.setNegativeButton("Later", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                Toast.makeText(MainActivity.this, "Product will be added later", Toast.LENGTH_LONG).show();
            }
        });
        builder.create().show();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case REQUEST_CODE_CAMERA:
                boolean shouldQuit = false;
                if (resultCode == CommonStatusCodes.SUCCESS) {
                    if (data != null) {
                        Toast.makeText(this, "Barcode detected ", Toast.LENGTH_SHORT).show();
                        Barcode barcode = data.getParcelableExtra("barcode");
                        viewProductCode.setText(barcode.displayValue);
                        currentBarcode = barcode.displayValue;
                        searchForProduct(currentBarcode);
                    } else {
                        Toast.makeText(this, "No Barcode detected ", Toast.LENGTH_SHORT).show();
                        shouldQuit = true;
                    }
                } else if (resultCode == CommonStatusCodes.CANCELED) {
                    Toast.makeText(this, "Scan Cancelled", Toast.LENGTH_SHORT).show();
                    shouldQuit = true;
                }
                if (shouldQuit){
                    finish();
                }
                break;
            case ADD_PRICE_INTENT:
                searchForProduct(currentBarcode);
                break;
            default:
                super.onActivityResult(requestCode, resultCode, data);
                break;

        }
    }

    private void searchForProduct(final String barcode){
        @SuppressLint("StaticFieldLeak")
        Fetcher fetcher = new Fetcher() {
            @Override
            protected void onFetched(String res) {
                if (res.equals("[]")){
//                    Toast.makeText(MainActivity.this, "Product is not in database", Toast.LENGTH_SHORT).show();
                    askToUpload(barcode);
                    return;
                }
                try {
                    JSONArray array = new JSONArray(res);
                         currentProduct = gson.fromJson(res, Product[].class)[0];

//                        JSONObject object = new JSONArray(res).getJSONObject(0);
                        // display values to views accordingly
                        Log.d("pipi", "onFetched: " + currentProduct.toString());
                        Log.d("pipi", "onFetched: " + currentProduct.getName());


                    Log.d(TAG, Arrays.toString(currentProduct.getPlaces()));
                    Log.d(TAG, "onFetched: " + currentProduct.getPlaces()[0].getName());
                    viewProductName.setText(currentProduct.getName());
                    currentImageUrl = currentProduct.getImageUrl();
                    lat = currentProduct.getPlaces()[0].getLat();
                    lon = currentProduct.getPlaces()[0].getLon();
                    displayShopInfo(currentProduct.getPlaces());
//                    viewProductCode.setText( String.valueOf(product.getCode()));
//                    viewProductPlace.setText(product.getPlaces()[0].getName());
//                    viewProductPrice.setText((int) product.getPlaces()[0].getPrice());
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        };
        String params = "code=" + barcode;
        Log.d("abc", params);
        fetcher.execute("http://188.166.209.81:3000/Product?", "GET", params);
    }

    private void askPermissions(){
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this,
                    new String[]{
                        Manifest.permission.CAMERA,
                        Manifest.permission.ACCESS_FINE_LOCATION,
                        Manifest.permission.READ_EXTERNAL_STORAGE,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE,
            }, REQUEST_PERMISSION_CODE_CAMERA);
            return;
        };
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, REQUEST_PERMISSION_CODE_LOCATION);
        };
    };

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if(requestCode == REQUEST_PERMISSION_CODE_LOCATION){
            uploadProductForm(currentBarcode);
        }
    }

    private void displayShopInfo(Shop[] shops){
        if (shops.length <= 0) return;
        ArrayList<HashMap<String,String>> arrayList=new ArrayList<>();
        for (Shop shop: shops){
            HashMap<String,String> hashMap=new HashMap<>();//create a hashmap to store the data in key value pair
            if (shop.getName() == null) continue;
            hashMap.put("name", shop.getName().split(",")[0]);
            hashMap.put("price", Double.toString(shop.getPrice()) + " VND");
            arrayList.add(hashMap);//add the hashmap into arrayList
        }
        String[] from={"name","price"};//string array
        int[] to={R.id.view_shop_name,R.id.view_product_price};//int array of views id's
        SimpleAdapter simpleAdapter=new SimpleAdapter(this,arrayList,R.layout.price_list,from,to);//Create object and set the parameters for simpleAdapter
        lv_shops.setAdapter(simpleAdapter);//sets the adapter for lv_shops
        registerForContextMenu(lv_shops);

    }

    @Override
    public void onCreateContextMenu(ContextMenu contextMenu, View view, ContextMenu.ContextMenuInfo menuInfo){
        super.onCreateContextMenu(contextMenu,view,menuInfo);
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.context_menu,contextMenu);
    }

    @Override
    public boolean onContextItemSelected(MenuItem menuItem){
        int selectedItemId = menuItem.getItemId();

        switch (selectedItemId){
            case R.id.report_context:
                try {
                    openMail(true);
                } catch (SecurityException e) {
                    Toast.makeText(this, "problem while opening Gmail directly", Toast.LENGTH_SHORT).show();
                    openMail(false);
                } catch (ActivityNotFoundException e) {
                    Toast.makeText(this, "problem while opening Gmail directly", Toast.LENGTH_SHORT).show();
                    openMail(false);
                }
                break;
        }
        return super.onContextItemSelected(menuItem);
    }

    private void openMail(boolean isGmail){
        Shop currentShop = currentProduct.getPlaces()[currentSelectedShopIndex];
        Intent mailIntent = new Intent(Intent.ACTION_SEND);
        mailIntent.setData(Uri.parse("mailto:"));
        mailIntent.setType("text/plain");
        if (isGmail){
            mailIntent.setClassName("com.google.android.gm", "com.google.android.gm.ComposeActivityGmail");
        }
        mailIntent.putExtra(Intent.EXTRA_EMAIL, new String[] { "pentest3330@gmail.com" });
        mailIntent.putExtra(Intent.EXTRA_SUBJECT, "Scan this: price report");
        mailIntent.putExtra(Intent.EXTRA_TEXT,
                "There is wrong price with the following product:"
                        + "\n name:  " + currentProduct.getName()
                        + "\n code:  " + currentProduct.getCode()
                        + "\n price: " + currentShop.getPrice()
                        + "\n place: " + currentShop.getName()
        );
        startActivity(mailIntent);
    }
}
