package com.example.leovo.scanthisbarcoder;

/**
 * Created by hainguyen on 2017-11-27.
 */

class Product {
    private String _id, name, desc, imageUrl, code;
    private Shop places[];

    public Product(String name, String desc, String imageUrl, String code, Shop[] places) {
        this.name = name;
        this.imageUrl = imageUrl;
        this.code = code;
        this.places = places;
        this.desc = desc;
    }

    public Product() {
    }

    String get_id() {
        return _id;
    }

    void set_id(String _id) {
        this._id = _id;
    }

    String getName() {
        return name;
    }

    void setName(String name) {
        this.name = name;
    }

    String getDesc() {
        return desc;
    }

    void setDesc(String desc) {
        this.desc = desc;
    }

    String getImageUrl() {
        return imageUrl;
    }

    void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    String getCode() {
        return code;
    }

    void setCode(String code) {
        this.code = code;
    }

    Shop[] getPlaces() {
        return places;
    }
}
