package com.example.leovo.scanthisbarcoder;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.DisplayMetrics;
import android.view.WindowManager;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;

/**
 * Created by LeoVo on 12/13/17.
 */

public class ViewProductPicActivity extends AppCompatActivity {
    ImageView imageView;
//    Button backHomeButton;
    String url;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_picture);


        url = getIntent().getStringExtra("image_url");
        imageView = (ImageView) findViewById(R.id.contextImage);
        loadImagefromURL (url);

        DisplayMetrics dm = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(dm);
        WindowManager.LayoutParams params = getWindow().getAttributes();
        params.alpha = 1f;    // lower than one makes it more transparent
        params.dimAmount = 0.7f;  // set it higher if you want to dim behind the window
        //0.1F
        int width = dm.widthPixels;
        int height = dm.heightPixels;

        getWindow().setLayout((int)(width*.8),(int)(height*.7));
        getWindow().setAttributes(params);
    }

    private void loadImagefromURL(String url) {
        Picasso.with(getApplicationContext())
                .load(url)
                .into(imageView);
    }

}
