package com.example.leovo.scanthisbarcoder;


import okhttp3.MultipartBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;

/**
 * Created by hainguyen on 2017-12-02.
 */

interface Service {
    @Multipart
    @POST("/image")
    Call<ResponseBody> postImage(@Part MultipartBody.Part image);
}
