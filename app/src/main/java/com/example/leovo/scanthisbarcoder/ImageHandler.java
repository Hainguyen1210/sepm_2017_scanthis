package com.example.leovo.scanthisbarcoder;

import android.app.Activity;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.content.FileProvider;
import android.util.Log;
import android.widget.Toast;

import java.io.File;

/**
 * Created by hainguyen on 2017-12-03.
 */

public class ImageHandler {
    private Uri mImageUri;
    private Context context;
    private Activity activity;
    private int REQUEST_CODE;
    private File file = null;
    String currentPhotoPath = "";
    final static String TAG = "pipi";
    public File getFile() {
        return file;
    }

    public Uri getmImageUri() {
        return mImageUri;
    }

    void setContext(Context context) {
        this.context = context;
    }

    void setmImageUri(Uri mImageUri) {
        this.mImageUri = mImageUri;
    }

    void setActivity(Activity activity) {
        this.activity = activity;
    }

    void setREQUEST_CODE(int REQUEST_CODE) {
        this.REQUEST_CODE = REQUEST_CODE;
    }

    ImageHandler(Context context, Activity activity, int REQUEST_CODE) {
        this.context = context;
        this.activity = activity;
        this.REQUEST_CODE = REQUEST_CODE;
    }

    void startIntent(){
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        // Ensure that there's a camera activity to handle the intent
        if (takePictureIntent.resolveActivity(context.getPackageManager()) != null) {
            // Create the File where the photo should go
            File photoFile = null;
            photoFile = createTemporaryFile();
            // Continue only if the File was successfully created
            if (photoFile != null) {
                Uri photoURI = FileProvider.getUriForFile(context,
                        "com.example.leovo.scanthisbarcoder",
                        photoFile);
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                activity.startActivityForResult(takePictureIntent, REQUEST_CODE);
            }
        }

//        intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
    }

    private File createTemporaryFile() {
        try {
            File tempDir = context.getExternalFilesDir(Environment.DIRECTORY_PICTURES);
            file = File.createTempFile("image",  ".jpg", tempDir);
            currentPhotoPath = file.getAbsolutePath();
            return file;
        } catch (Exception e) {
            Toast.makeText(context, "Check your storage", Toast.LENGTH_SHORT).show();
            Log.d(TAG, "createTemporaryFile: "  + e);
            e.printStackTrace();
        }
        return null;
    }

    void deleteTempFile(){
        if (file != null){
            Log.d(TAG, "deleteTempFile: " + "trying to delete" + file.toURI().toString());
//            Log.d(TAG, "deleteTempFile: " + file.delete());
        }
    }

//    Bitmap getImage() {
//        context.getContentResolver().notifyChange(mImageUri, null);
//        ContentResolver cr = context.getContentResolver();
//        Bitmap bitmap;
//        try {
//            return android.provider.MediaStore.Images.Media.getBitmap(cr, mImageUri);
//        } catch (Exception e) {
//            Toast.makeText(context, "Failed to load", Toast.LENGTH_SHORT).show();
//            Log.d(TAG, "Failed to load", e);
//            return null;
//        }
//    }
//
//    @Nullable
//    Uri getImageUri(Context context, Bitmap bitmap) {
//        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
//        bitmap.compress(Bitmap.CompressFormat.JPEG, 0, bytes);
//        String path = MediaStore.Images.Media.insertImage(context.getContentResolver(), bitmap, "Title", null);
//        return Uri.parse(path);
//    }
//
//    String getRealPathFromUri() {
//        Cursor cursor = null;
//        try {
//            String[] proj = { MediaStore.Images.Media.DATA };
//            cursor = context.getContentResolver().query(getImageUri(context, getImage()), proj, null, null, null);
//            int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
//            cursor.moveToFirst();
//            return cursor.getString(column_index);
//        } finally {
//            if (cursor != null) {
//                cursor.close();
//            }
//        }
//    }
}
