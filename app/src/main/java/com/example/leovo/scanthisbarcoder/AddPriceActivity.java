package com.example.leovo.scanthisbarcoder;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlacePicker;
import com.squareup.picasso.Picasso;

public class AddPriceActivity extends AppCompatActivity {
    private String productName, productCode;
    private TextView viewProductName, viewProductCode, viewProductAddress;
    private EditText editTextProductPrice;
    private Button buttonSelectLocation, buttonSubmit;
    private boolean
            isOpeningPlacePicker = false,
            isLocationEmpty = true,
            isImageEmpty = true;;
    private final String TAG = "pipi";
    private Shop addingShop = new Shop();
    private ImageView imageLocation;
    private ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_price);
        productCode = getIntent().getStringExtra(MainActivity.PUTEXTRA_BARCODE);
        productName = getIntent().getStringExtra(MainActivity.PUTEXTRA_NAME);
        Log.d(TAG, "onCreate: " + productName);
        viewProductCode = (TextView) findViewById(R.id.view_product_code);
        viewProductName = (TextView) findViewById(R.id.view_product_name);
        viewProductAddress = (TextView) findViewById(R.id.view_location_address);
        editTextProductPrice = (EditText) findViewById(R.id.edit_product_price);
        buttonSelectLocation = (Button) findViewById(R.id.button_select_location);
        buttonSubmit = (Button) findViewById(R.id.button_submit);
        imageLocation = (ImageView) findViewById(R.id.image_location);
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Uploading product");

        viewProductCode.setText(productCode);
        viewProductName.setText(productName);
        buttonSelectLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                pickLocation();
            }
        });
        buttonSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                submitProduct();
            }
        });
        editTextProductPrice.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                editTextProductPrice.setHint(b? "":"VND");
            }
        });

    }

    private void pickLocation() {
        if (isOpeningPlacePicker) return;
        isOpeningPlacePicker = true;
        isLocationEmpty = true;
        if (ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED){
            try {
                PlacePicker.IntentBuilder builder = new PlacePicker.IntentBuilder();
                startActivityForResult(builder.build(this), MainActivity.REQUEST_PERMISSION_CODE_LOCATION);
            } catch (GooglePlayServicesRepairableException e) {
                Log.d(TAG, "pickLocation: ");
                e.printStackTrace();
            } catch (GooglePlayServicesNotAvailableException e) {
                Log.d(TAG, "pickLocation: ");
                e.printStackTrace();
            }
        } else {
            finish();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case MainActivity.REQUEST_PERMISSION_CODE_LOCATION:
                isOpeningPlacePicker = false;
                if (resultCode == RESULT_OK) {
                    Place place = PlacePicker.getPlace(this, data);
                    String address = place.getName() + ", " + place.getAddress();
                    if (place.getName().toString().contains("°")) {
                        Toast.makeText(this, "Tap on a shop to get exact name", Toast.LENGTH_LONG).show();
                        pickLocation();
                    } else {
                        addingShop.setName(address);
                        addingShop.setLat(place.getLatLng().latitude);
                        addingShop.setLon(place.getLatLng().longitude);
                        Picasso.with(getApplicationContext())
                                .load("https://maps.googleapis.com/maps/api/staticmap?center="
                                        + addingShop.getLat() + ","
                                        + addingShop.getLon() + "&zoom=18&size=800x400&key=AIzaSyBvN1dMwPYBGdaJ42U8S4GNM7UVlk_LHAc")
                                .into(imageLocation);
                        viewProductAddress.setText(address);
                        isLocationEmpty = false;
                    }
                }
                break;
        }
    }

    private void submitProduct() {
        if (editTextProductPrice.getText().toString().equals("")) {
            editTextProductPrice.requestFocus();
            return;
        } else {
            addingShop.setPrice(Double.parseDouble(editTextProductPrice.getText().toString()));
        }
        if (isLocationEmpty){
            Toast.makeText(this, "Taking Location", Toast.LENGTH_SHORT).show();
            pickLocation();
            return;
        }
        progressDialog.show();
        @SuppressLint("StaticFieldLeak") Fetcher fetcher = new Fetcher() {
            @Override
            protected void onFetched(String res) {
                Toast.makeText(AddPriceActivity.this, res, Toast.LENGTH_SHORT).show();
                progressDialog.dismiss();
                Log.d(TAG, "onFetched: " + res);
                if (res.contains("new price added")){
                    Intent intent = new Intent();
                    setResult(MainActivity.ADD_PRICE_INTENT, intent);
                    finish();
                }
            }
        };
        StringBuilder paramsBuilder = new StringBuilder();
        paramsBuilder.append("code=").append(productCode);
        paramsBuilder.append("&placeName=").append(addingShop.getName());
        paramsBuilder.append("&price=").append(addingShop.getPrice());
        paramsBuilder.append("&lat=").append(addingShop.getLat());
        paramsBuilder.append("&lon=").append(addingShop.getLon());
        Log.d(TAG, "submitProduct: " + paramsBuilder.toString());
        fetcher.execute("http://188.166.209.81:3000/updatePrice", "POST", paramsBuilder.toString());
    }

}
