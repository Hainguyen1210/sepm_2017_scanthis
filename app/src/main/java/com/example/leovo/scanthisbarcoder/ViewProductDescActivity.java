package com.example.leovo.scanthisbarcoder;

import android.app.Activity;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.WindowManager;
import android.widget.TextView;
import android.widget.Toast;

/**
 * Created by LeoVo on 12/21/17.
 */

public class ViewProductDescActivity extends Activity {
    final String TAG = "pipi";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        String descFromIntent = getIntent().getStringExtra(MainActivity.PUTEXTRA_DESC);
        if (descFromIntent.length() < 1) {
            Toast.makeText(this, "Product doesn't have description", Toast.LENGTH_SHORT).show();
            finish();
        }

        setContentView(R.layout.activity_view_descriptions);

        DisplayMetrics dm = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(dm);
        WindowManager.LayoutParams params = getWindow().getAttributes();
        params.alpha = 1f;    // lower than one makes it more transparent
        params.dimAmount = 0.7f;  // set it higher if you want to dim behind the window
        //0.1F
        int width = dm.widthPixels;
        int height = dm.heightPixels;

        getWindow().setLayout((int)(width*.8),(int)(height*.7));
        getWindow().setAttributes(params);

        TextView viewProductDesc = (TextView) findViewById(R.id.view_product_desc);
        Log.d(TAG, "onCreate: " + descFromIntent);
        Log.d(TAG, "onCreate: " + viewProductDesc.getText());

        viewProductDesc.setText(descFromIntent);


    }

}
