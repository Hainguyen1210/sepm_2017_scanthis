package com.example.leovo.scanthisbarcoder;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

/**
 * Created by LeoVo on 11/21/17.
 */

public class StartupActivity extends AppCompatActivity {
    ProgressBar progressBar;


    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.startup_main_scan );
        ImageView iv = (ImageView) findViewById(R.id.loadlogo);
        TextView iv2 = (TextView) findViewById(R.id.txv);
         progressBar = (ProgressBar) findViewById(R.id.progressBar3);

        Animation animation = AnimationUtils.loadAnimation(this,R.anim.startup_animate);
        iv.startAnimation(animation);
        iv2.startAnimation(animation);
        final Intent i = new Intent(this,MainActivity.class);
        Thread timer = new Thread() {
            public void run() {
                for (int j = 0; j <= 100 ; j++) {
                    progressBar.setProgress(j);
                    j+=10;
                }
                try {
                    sleep(1000);
                } catch (InterruptedException e){
                    e.printStackTrace();
                }
                finally {
                    startActivity(i) ;
                    finish();
                }
            }
        };
            timer.start();
    }
}
