package com.example.leovo.scanthisbarcoder;

/**
 * Created by hainguyen on 2017-11-27.
 */

public class Shop {
    private double price, lat, lon;
    private String name, region;

    public Shop(double price, double lat, double lon, String name, String region) {
        this.price = price;
        this.lat = lat;
        this.lon = lon;
        this.name = name;
    }

    public Shop() {
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public double getLon() {
        return lon;
    }

    public void setLon(double lon) {
        this.lon = lon;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }
}
