package com.example.leovo.scanthisbarcoder;

/**
 * Created by LeoVo on 11/23/17.
 */

import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;
import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;


public abstract class Fetcher extends AsyncTask<String,Void,String> {
    private String boundary =  "SwA"+Long.toString(System.currentTimeMillis())+"SwA";
    protected abstract void onFetched(String res);
    final String TAG = "pipi";

    @Override
    protected void onPostExecute(String s) {
        onFetched(s);
    }

    @Override
    protected String doInBackground(String... strings) {
        return makeHttpRequest(strings[0], strings[1], strings[2]);
    }

    private String makeHttpRequest(String urlStr, String method, String paramsStr){
        StringBuilder response = new StringBuilder();
        try {
            URL url = new URL(urlStr + (method.equals("GET") ? paramsStr:""));
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            if (method.equals("POST")){
                connection.setRequestMethod( "POST" );
                connection.setRequestProperty( "Content-Type", "application/json");
                DataOutputStream dataOutputStream = new DataOutputStream(connection.getOutputStream());
                JSONObject jsonObject = new JSONObject();
                String[] params = paramsStr.split("&");
                for (String param : params){
                    String[] parts = param.split("=");
                    jsonObject.put(parts[0],parts[1]);
                }
                Log.d(TAG, "makeHttpRequest: " + jsonObject.toString());
                dataOutputStream.write(jsonObject.toString().getBytes("UTF-8")) ;
            }
            BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));

            String line = "";

            do {
                response.append(line);
                line = reader.readLine();
            }
            while (line != null);
            connection.disconnect();

        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return response.toString();
    }

}
